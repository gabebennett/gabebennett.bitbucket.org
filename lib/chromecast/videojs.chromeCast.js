vjs.debug = false;

vjs.debugLog = function(message)
{
    if(vjs.debug)
    {
        console.log(message);
    }
}

/*! videojs-chromeCast - v0.1.0 - 2014-02-20
* https://github.com/benjipott/videojs-chromeCast
* Copyright (c) 2014 Pott Benjamin; Licensed MIT */
/**
 * Merge two objects together and return the original.
 *
 * @param {Object}
 *                obj1
 * @param {Object}
 *                obj2
 * @return {Object}
 */
vjs.plugin.merge = function(obj1, obj2) {
    var settings = vjs.obj.merge.apply(this,arguments);
    if(settings.hasOwnProperty('userAgentAllowed') && settings.enabled){
        settings.userAgentAllowed = settings.userAgentAllowed.split(',');
        for ( var a = 0, b = settings.userAgentAllowed; a < b.length; a++) {
            var ualist = new RegExp(b[a],'i');
            settings.enabled = !!vjs.USER_AGENT.match(ualist);
            if (settings.enabled){
                break;
            }
        }
    }
    return settings;
};

(function() {
    var defaults = {
        enabled: true,
        namespace: '',
        title: '',
        description: ''
    };

    vjs.plugin('chromecast', function(options) {
        var settings = vjs.plugin.merge(defaults, options);

        if (!settings.enabled) {
            return false;
        }

        this.player = this;

        this.chromeCastComponent = new vjs.ChromeCastComponent(this, settings);
        this.player.controlBar.addChild(this.chromeCastComponent);
    });


})();
/**
 * Button pour envoyer le flux a ChromeCast
 *
 * @param {vjs.Player|Object} player
 * @param {Object=} options
 * @constructor
 */
var cast = cast || {};

vjs.Player.prototype.chromeCastComponent = {};

vjs.ChromeCastComponent = vjs.Button.extend({
    /** @constructor */
    init: function(player, options) {
        this.settings = options;
        vjs.Button.call(this, player, options);

        if (!player.controls()) {
            this.disable();
        }

        this.hide();
        this.el_.setAttribute('role', 'button');

        this.initializeApi();

    }

});

vjs.ChromeCastComponent.prototype.kind_ = 'chromecast';
vjs.ChromeCastComponent.prototype.buttonText = 'Chromecast';
vjs.ChromeCastComponent.prototype.className = 'vjs-chromecast-button ';
vjs.ChromeCastComponent.prototype.chromeCastBanner = {};


vjs.ChromeCastComponent.prototype.apiMedia = null;
vjs.ChromeCastComponent.prototype.apiSession = null;
vjs.ChromeCastComponent.prototype.apiInitialized = false;

vjs.ChromeCastComponent.prototype.casting = false;
vjs.ChromeCastComponent.prototype.progressFlag = 1;
vjs.ChromeCastComponent.prototype.timer = null;
vjs.ChromeCastComponent.prototype.timerStep = 1000;

vjs.ChromeCastComponent.prototype.currentMediaTime = 0;
vjs.ChromeCastComponent.prototype.paused = true;
vjs.ChromeCastComponent.prototype.seeking = false;
vjs.ChromeCastComponent.prototype.currentVolume = 1;
vjs.ChromeCastComponent.prototype.muted = false;

vjs.ChromeCastComponent.boundEvents = {};


vjs.ChromeCastComponent.prototype.onInitSuccess = function() {
    vjs.debugLog("api_status :Initialized");

    this.apiInitialized = true;
};

vjs.ChromeCastComponent.prototype.onInitError = function(castError) {
    vjs.debugLog("Initialize Error: " + JSON.stringify(castError));
};

vjs.ChromeCastComponent.prototype.sessionJoinedListener = function(session) {
    vjs.debugLog("Joined " + session.sessionId);
};

vjs.ChromeCastComponent.prototype.sessionUpdateListener = function(isAlive) {
    var message = isAlive ? 'Session Updated' : 'Session Removed';
    vjs.debugLog(message);
    if(!isAlive)
    {
        this.stopApp();
    }
};

vjs.ChromeCastComponent.prototype.receiverListener = function(availability) {
    vjs.debugLog("receivers available: " + (('available' === availability) ? "Yes" : "No"));
    if ('available' === availability) {
        this.show();
    }
};

vjs.ChromeCastComponent.prototype.initializeApi = function() {
    vjs.debugLog('ChromeCastComponent initializeApi');

    if (!vjs.IS_CHROME) {
        return;
    }

    if (!chrome.cast || !chrome.cast.isAvailable) {
        vjs.debugLog("Cast APIs not Available. Retrying...");
        setTimeout(this.initializeApi.bind(this), 1000);
        return;
    }

    var sessionRequest;
    if (this.settings.appId) {
        var sessionRequest = new chrome.cast.SessionRequest(this.settings.appId);
    } else {
        var sessionRequest = new chrome.cast.SessionRequest(chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID);
    }

    var apiConfig = new chrome.cast.ApiConfig(sessionRequest,
        this.sessionJoinedListener.bind(this),
        this.receiverListener.bind(this));
    chrome.cast.initialize(apiConfig, this.onInitSuccess.bind(this), this.onInitError.bind(this));
};

vjs.ChromeCastComponent.prototype.doLaunch = function() {
    //vjs.debugLog("Cast video : " + this.player_.currentSrc());
    if (this.apiInitialized) {
        this.player_.pause();
        var errorCallback = function(castError) {
            vjs.debugLog("session_established ERROR: " + JSON.stringify(castError));
            this.player_.play();
        };
        chrome.cast.requestSession(
                // Success
                this.onSessionSuccess.bind(this),
                // Error
                errorCallback.bind(this)
            );
    } else {
        vjs.debugLog("session_established NOT INITIALIZED");
    }
};

vjs.ChromeCastComponent.prototype.onSessionSuccess = function(session) {
    this.apiSession = session;
    //vjs.debugLog("session_established YES - " + session.sessionId);
    this.addClass("connected");
    session.addUpdateListener(this.sessionUpdateListener.bind(this));
    this.player_.chromeCastComponent.loadMedia(this.player_.currentSrc(), this.player_.currentTime());
};

vjs.ChromeCastComponent.prototype.loadMedia = function(src, time) {    
    if(typeof time == undefined)
    {
        time = 0;
    }
    var mediaInfo = new chrome.cast.media.MediaInfo(src, "video/mp4");
    var loadRequest = new chrome.cast.media.LoadRequest(mediaInfo);
    loadRequest.autoplay = true;
    loadRequest.currentTime = time;
    // vjs.debugLog('Sending Load Request: ');
    // vjs.debugLog(loadRequest);
    
    this.apiSession.loadMedia(loadRequest, this.onMediaDiscovered.bind(this), this.onMediaError.bind(this));
};

vjs.ChromeCastComponent.prototype.onMediaDiscovered = function(media) {
    // chrome.cast.media.Media object
    this.apiMedia = media;
    this.apiMedia.addUpdateListener(this.onMediaStatusUpdate.bind(this));

    //vjs.debugLog("Got media object");
    this.startProgressTimer(this.incrementMediaTime.bind(this));

    //vjs.debugLog("play!!!!");
    if(!this.casting)
    {
        this.paused = false;
        this.player_.loadTech('ChromecastTech', {});
        this.player_.userActive(true);
        this.casting = true;
    }
};

vjs.ChromeCastComponent.prototype.onMediaError = function(castError) {
    vjs.debugLog('Media Error: ' + JSON.stringify(castError));
};

vjs.ChromeCastComponent.prototype.onMediaStatusUpdate = function(e) {

    if (!this.apiMedia) {
        return;
    }

    if (this.progressFlag) {
        // vjs.debugLog(parseInt(100 * this.apiMedia.currentTime / this.apiMedia.media.duration) + "%");
        // vjs.debugLog(this.apiMedia.currentTime + "/" + this.apiMedia.media.duration);
    }

    vjs.ChromeCastComponent.prototype.currentMediaTime = this.apiMedia.currentTime;

    vjs.debugLog(this.apiMedia.playerState);
};

vjs.ChromeCastComponent.prototype.startProgressTimer = function(callback) {
    if (this.timer) {
        clearInterval(this.timer);
        this.timer = null;
    }
    vjs.debugLog('starting timer...');
    // start progress timer
    this.timer = setInterval(callback.bind(this), this.timerStep);
};

vjs.ChromeCastComponent.prototype.duration = function() {
    if (!this.apiMedia) {
        return 0;
    }

    return this.apiMedia.media.duration;
};

/**
 * play media
 */


vjs.ChromeCastComponent.prototype.play = function() {
    if (!this.apiMedia) {
        return;
    }

    if (this.paused) {
        this.apiMedia.play(null,
            this.mediaCommandSuccessCallback.bind(this, "playing started for " + this.apiMedia.sessionId),
            this.onError.bind(this));
        this.apiMedia.addUpdateListener(this.onMediaStatusUpdate.bind(this));

        this.paused = false;
    }
};

vjs.ChromeCastComponent.prototype.pause = function() {
    if (!this.apiMedia) {
        return;
    }

    if (!this.paused) {
        this.apiMedia.pause(null,
            this.mediaCommandSuccessCallback.bind(this, "paused " + this.apiMedia.sessionId),
            this.onError.bind(this));

        this.paused = true;
    }
};

/**
 * seek media position
 * @param {Number} pos A number to indicate percent
 */
vjs.ChromeCastComponent.prototype.seekMedia = function(pos) {
    //vjs.debugLog('Seeking ' + currentMediaSession.sessionId + ':' + currentMediaSession.mediaSessionId + ' to ' + pos + "%");
    //progressFlag = 0;
    var request = new chrome.cast.media.SeekRequest();
    request.currentTime = pos;
    this.apiMedia.seek(request,
        this.onSeekSuccess.bind(this, pos),
        this.onError);
};


vjs.ChromeCastComponent.prototype.onSeekSuccess = function(pos) {
    this.currentMediaTime = pos;
};

vjs.ChromeCastComponent.prototype.setMediaVolume = function(level, mute) {
    if (!this.apiMedia)
        return;

    var volume = new chrome.cast.Volume();
    volume.level = level;
    this.currentVolume = volume.level;
    volume.muted = mute;
    this.muted = mute;
    var request = new chrome.cast.media.VolumeRequest();
    request.volume = volume;
    this.apiMedia.setVolume(request,
        this.mediaCommandSuccessCallback.bind(this, 'media set-volume done'),
        this.onError);
    this.player_.trigger('volumechange');
};

vjs.ChromeCastComponent.prototype.incrementMediaTime = function() {
    if (this.apiMedia.playerState === chrome.cast.media.PlayerState.PLAYING) {
        if (this.currentMediaTime < this.apiMedia.media.duration) {
            this.currentMediaTime += 1;
            this.trigger("timeupdate");
        } else {
            this.currentMediaTime = 0;
            clearInterval(this.timer);
        }
    }
};

/**
 * Callback function for media command success
 */
vjs.ChromeCastComponent.prototype.mediaCommandSuccessCallback = function(info, e) {
    vjs.debugLog(info);
};

vjs.ChromeCastComponent.prototype.onError = function() {
    vjs.debugLog("error");
    this.stopApp();
};

/**
 * Stops the running receiver application associated with the session.
 */
vjs.ChromeCastComponent.prototype.stopCasting = function() {
    this.apiSession.stop(this.onStopAppSuccess.bind(this),
        this.onError.bind(this));
};

/**
 * Callback function for stop app success
 */
vjs.ChromeCastComponent.prototype.onStopAppSuccess = function() {
    this.stopApp();
};

vjs.ChromeCastComponent.prototype.stopApp = function() {
    if(!this.casting || !this.apiMedia)
    {
        return;
    }
    clearInterval(this.timer);
    this.casting = false;
    this.removeClass("connected");
    this.player_.src(this.player_.options_['sources']);
    vjs.debugLog("stop and set sources to");
    vjs.debugLog(this.player_.options_['sources']);
    vjs.insertFirst(this.player_.tech.el_, this.player_.el());
    
    if (this.apiMedia.playerState == "IDLE") {
        this.player_.currentTime(0);
        this.player_.onPause();
    } else {
        this.player_.currentTime(this.currentMediaTime);
        if (!this.paused) {
            this.player_.play();
        }
    }
    this.apiMedia = null;
};

vjs.ChromeCastComponent.prototype.buildCSSClass = function() {
    return this.className + vjs.Button.prototype.buildCSSClass.call(this);
};

vjs.ChromeCastComponent.prototype.createEl = function(type, props) {
    var el = vjs.Button.prototype.createEl.call(this, 'div');
    return el;
};

vjs.ChromeCastComponent.prototype.onClick = function() {
    vjs.Button.prototype.onClick.call(this);

    if (this.casting) {
        this.stopCasting();
    } else {
        this.doLaunch();
    }
};

vjs.ChromeCastBanner = vjs.Component.extend({
    /** @constructor */
    init: function(player, options) {
        vjs.Component.call(this, player, options);
    }

});

vjs.ChromeCastBanner.prototype.createEl = function(type, props) {
    props = vjs.obj.merge({
        className: this.buildCSSClass(),
        innerHTML: '',
        tabIndex: 0
    }, props);

    return vjs.Component.prototype.createEl.call(this, type, props);

};

vjs.ChromeCastBanner.prototype.buildCSSClass = function() {
    return 'vjs-currentlyCasting';
};

vjs.ChromecastTech = vjs.MediaTechController.extend({
    /** @constructor */
    init: function(player, options, ready) {

        this.features=[];
        this.features['volumeControl'] = true;
        this.features['movingMediaElementInDOM'] = false;
        this.features['fullscreenResize'] = false;
        this.features['progressEvents'] = true;
        this.features['timeupdateEvents'] = true;

        vjs.MediaTechController.call(this, player, options, ready);

        var source = options['source'];

        vjs.debugLog("ChromecastTech initialized...");

        this.el_ = videojs.Component.prototype.createEl('div', {
            id: 'myId',
            className: 'vjs-tech',
            innerHTML: '<img src="' + this.player_.options_.poster + '" class="backgroundImage"/><div class="currentlyCasting"><h2 class="castingLabel">Casting to device</h2></div>'
        });
        vjs.insertFirst(this.el_, this.player_.el());
        this.triggerReady();
    }
});

vjs.ChromecastTech.apiSession = {};
vjs.ChromecastTech.apiMedia = {};

vjs.ChromecastTech.isSupported = function() {
    vjs.debugLog("isSupported");
    return this.player_.chromeCastComponent.apiInitialized;
};

videojs.ChromecastTech.canPlaySource = function(srcObj) {
    vjs.debugLog("canPlaySource");
    return (srcObj.type == 'video/mp4');
};

vjs.ChromecastTech.prototype.dispose = function() {
    vjs.MediaTechController.prototype.dispose.call(this);
};

vjs.ChromecastTech.prototype.play = function() {
    vjs.debugLog("play");
    this.player_.chromeCastComponent.play();
    this.player_.onPlay();
};

vjs.ChromecastTech.prototype.pause = function() {
    vjs.debugLog("pause");
    this.player_.chromeCastComponent.pause();
    this.player_.onPause();
};

vjs.ChromecastTech.prototype.paused = function() {
    vjs.debugLog("paused?");
    return this.player_.chromeCastComponent.paused;
};

vjs.ChromecastTech.prototype.currentTime = function() {
    vjs.debugLog("currentTime?");
    return this.player_.chromeCastComponent.currentMediaTime;
};

vjs.ChromecastTech.prototype.setCurrentTime = function(seconds) {
    vjs.debugLog("setCurrentTime: " + seconds);
    this.player_.chromeCastComponent.seekMedia(seconds);
};

vjs.ChromecastTech.prototype.duration = function() {
    vjs.debugLog("duration");
    return 0;
};

vjs.ChromecastTech.prototype.buffered = function() {
    vjs.debugLog("buffered");
    return {
        length: 0
    };
};

vjs.ChromecastTech.prototype.volume = function() {
    return this.player_.chromeCastComponent.currentVolume;
};

vjs.ChromecastTech.prototype.setVolume = function(percentAsDecimal) {
    vjs.debugLog("setVolume: " + percentAsDecimal);
    this.player_.chromeCastComponent.setMediaVolume(percentAsDecimal, false);
};

vjs.ChromecastTech.prototype.muted = function() {
    vjs.debugLog("muted?");
    return this.player_.chromeCastComponent.muted;
};

vjs.ChromecastTech.prototype.setMuted = function(muted) {
    vjs.debugLog("setMuted: " + muted);
    this.player_.chromeCastComponent.setMediaVolume(this.player_.chromeCastComponent.currentVolume, muted);
};

vjs.ChromecastTech.prototype.supportsFullScreen = function() {
    vjs.debugLog("supportsFullScreen?");
    return false;
};

vjs.ChromecastTech.prototype.enterFullScreen = function() {
    vjs.debugLog("enterFullScreen");
};

vjs.ChromecastTech.prototype.exitFullScreen = function() {
    vjs.debugLog("exitFullScreen");
};

vjs.ChromecastTech.prototype.src = function(src) {
    vjs.debugLog("ChromecastTech src: " + src);
    this.player_.chromeCastComponent.loadMedia(this.player_.currentSrc(), 0);
};

vjs.ChromecastTech.prototype.load = function() {
    vjs.debugLog("load");
};

vjs.ChromecastTech.prototype.currentSrc = function() {
    vjs.debugLog("currentSrc?");
    return "";
};

vjs.ChromecastTech.prototype.poster = function() {
    vjs.debugLog("poster?");
};

vjs.ChromecastTech.prototype.setPoster = function(val) {    
    this.el_.getElementsByTagName('img')[0].setAttribute('src', val);
    vjs.debugLog("setPoster: " + val);
};

vjs.ChromecastTech.prototype.preload = function() {
    vjs.debugLog("preload?");
    return true;
};

vjs.ChromecastTech.prototype.setPreload = function(val) {
    vjs.debugLog("setPreload: " + val);
    /*this.el_.preload = val;*/
};

vjs.ChromecastTech.prototype.autoplay = function() {
    /*return this.el_.autoplay;*/
    vjs.debugLog("autoplay?");
    return true;
};

vjs.ChromecastTech.prototype.setAutoplay = function(val) {
    /*this.el_.autoplay = val;*/
};

vjs.ChromecastTech.prototype.controls = function() {
    vjs.debugLog("controls?");
    return true;
};

vjs.ChromecastTech.prototype.setControls = function(val) {
    vjs.debugLog("setControls: " + val);
};

vjs.ChromecastTech.prototype.loop = false;
vjs.ChromecastTech.prototype.setLoop = function(val) {};

vjs.ChromecastTech.prototype.error = function() {
    return false;
};

vjs.ChromecastTech.prototype.seeking = function() {
    vjs.debugLog("seeking?");
    return false;
};

vjs.ChromecastTech.prototype.ended = function() {
    vjs.debugLog("ended?");
    return false;
};

vjs.ChromecastTech.prototype.defaultMuted = false;