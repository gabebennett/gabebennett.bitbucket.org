'use strict';

app.factory('DvidsAPIdata', function ($resource, DVIDS_API_KEY) {
    var baseParams = { api_key: DVIDS_API_KEY };
    var goApi = $resource('//api.dvidshub.net/:sub1/:sub2/:sub3', baseParams, {'query': { method: 'GET', isArray: false }});
    return goApi;
  });