'use strict';

app.filter("allow_html", ['$sce', function ($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);


app.filter("toArray", function(){
  return function(obj) {
    var result = [];
    angular.forEach(obj, function(val, key) {
      result.push(val);
    });
    return result;
  };
});


app.filter('trimDescription', function() {
    return function(input) {
    if (input.length > 110) {
        var filteredText = input.substring(0, 100).split(" ").slice(0, -1).join(" ") + '...';
      } else {
        var filteredText = input;
      }
      return filteredText;
    };
  });

app.filter('trimTitle', function() {
    return function(input) {
    if (input.length > 80) {
        var filteredText = input.substring(0, 80).split(" ").slice(0, -1).join(" ") + '...';
      } else {
        var filteredText = input;
      }
      return filteredText;
    };
  });