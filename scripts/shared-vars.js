'use strict';

app.service('StoredVariables', function StoredVariables() {

    var currentPlayerVideoID = '';
    
    return {
        setPlayerVideoID : function(PlayerVideoID) {
            currentPlayerVideoID = PlayerVideoID;
        },
        getPlayerVideoID : function() {
            return currentPlayerVideoID;
        }

    };

  });