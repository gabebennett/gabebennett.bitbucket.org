'use strict';

app.controller('VideoPlayerCtrl', function ($scope, $rootScope, $q, $sce, DvidsAPIdata, StoredVariables, $timeout) {

/* All below is for handling the video player
------------------------------------------------------------------------------ */
    $scope.videoAsset = [];
    $rootScope.LiveVideoAsset = [];
    $rootScope.areWeLive = false;
    $rootScope.notFoundMessage;

    
    $timeout(function() {
        $scope.videoID = StoredVariables.getPlayerVideoID();
      }, 30);
    console.log($scope.videoID);

    

    // Needs to be passed from buttons on left
    // $rootScope.videoID = 9572;

    // Format for previously ingested videoID 
    $scope.videoID = 'video:484802';

    // Use this to fetch a previously ingested video
        
        var searchParams = { sub1: 'asset', id: $scope.videoID, fields: 'id,title,date,description,duration,files,hls_url,time_start,closed_caption_urls' };
        $rootScope.ingestedVideo = true;
        

    // for live fetch
    /*
    var searchParams = { sub1: 'live', sub2: 'get', id: $scope.videoID };
     */   

    //  function getVideo(searchParams) {
    var getVideo = function(searchParams) {
      var defer = $q.defer();
      // Send a request to dvidsAPIdata factory (services/dvids-api-data.js)
      DvidsAPIdata.get(searchParams, function(response) {
        // Success!
        $scope.videoAsset = response.results;
        $rootScope.LiveVideoAsset = response.results;

        //console.log(response.results);



        /*  Video.js stuff
        ------------------------------------------------------------------------------ */

        // Set the video source variable
        if (typeof response.results.hls_url == 'undefined' || response.results.hls_url == '') {
            var video_src = response.results.files[0].src;
        } else {
            var video_src = response.results.hls_url;
        }

        var vidArgs = [video_src];


        myPlayer = videojs('video_container',
                {
                    "controls": true,
                    "autoplay": false,
                    "preload": "auto",
                    "techOrder": ["flash", "html5", "ChromecastTech"],
                    "plugins": {'chromecast': {appId: '04D1E7AD'}}
                }
            );

      },

      function(response){
        // Fail.
        var vidArgs = ['failure'];
        $scope.$onFailure(vidArgs);
      });
      return defer.promise;



    } // end getVideo()

    getVideo(searchParams);


  });
