'use strict';

app.controller('MainCtrl', function ($scope, $rootScope, $q, DvidsAPIdata, StoredVariables, $timeout) {

    /* Get API live data
    ------------------------------------------------------------------------------------- */

    // $scope.currentSearchTerms = null;
    var searchParams = { sub1: 'live', sub2: 'list', sort: 'begin', max_results: 4 };
    
    // Go get the Data!
    DvidsAPIdata.query(searchParams, function(response) {
      // Assign the response
      $scope.videoList = response.results;
      console.log($scope.videoList);

      // First video set to go to player
      setPlayerVidID(response.results[0].id);
      $scope.check = StoredVariables.getPlayerVideoID();
      console.log($scope.check);

      //console.log($scope.currentVideoInPlayer);

      // Check if there is a live stream at the moment. If so, play it.
      var now = Date.now();
      var time1 = Date.parse(response.results[0].begin);
      var time2 = Date.parse(response.results[0].end);

      if (now > time1 && now < time2) {
          $rootScope.areWeLive = true;
          $scope.alertClosed = true;
      } else {
          $rootScope.areWeLive = false;
      }

    });

    // This is to toggle "Live Now!" on currently live thumbs
    $scope.liveCheck2 = function(date){
        var parsedDate = Date.parse(date);
        var nowTime = Date.now();

        // if this is a live event activate the "live_thumb" CSS class
        return {'show': parsedDate < nowTime, 'hide': parsedDate > nowTime};
    }

    function setPlayerVidID (vidID){
        StoredVariables.setPlayerVideoID(vidID);
      };
    

});